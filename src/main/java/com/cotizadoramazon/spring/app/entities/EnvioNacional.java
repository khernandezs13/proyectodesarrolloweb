package com.cotizadoramazon.spring.app.entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.dom4j.tree.AbstractEntity;

@Entity
@Table(name = "Envio_Nacional")
public class EnvioNacional extends AbstractEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	
	private Long id;
	private double envioNacional;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public double getEnvioNacional() {
		return envioNacional;
	}
	public void setEnvioNacional(double envioNacional) {
		this.envioNacional = envioNacional;
	}
	
	
}
