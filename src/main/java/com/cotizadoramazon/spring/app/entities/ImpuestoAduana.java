package com.cotizadoramazon.spring.app.entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.dom4j.tree.AbstractEntity;

@Entity
@Table(name = "Impuesto_Aduana")
public class ImpuestoAduana extends AbstractEntity implements Serializable {
	
private static final long serialVersionUID = 1L;
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	
	
	private Long id;
	private double cantImpuestoAduana;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public double getCantImpuestoAduana() {
		return cantImpuestoAduana;
	}
	public void setCantImpuestoAduana(double cantImpuestoAduana) {
		this.cantImpuestoAduana = cantImpuestoAduana;
	}
	
	
}
