package com.cotizadoramazon.spring.app.repositories;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.cotizadoramazon.spring.app.entities.CostoEnvioInternacional;

@Repository
public interface CostoEnvioInternacionalRepository extends PagingAndSortingRepository<CostoEnvioInternacional, Long> {

}
