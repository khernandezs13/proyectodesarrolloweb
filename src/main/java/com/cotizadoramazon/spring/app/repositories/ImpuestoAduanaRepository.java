package com.cotizadoramazon.spring.app.repositories;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.cotizadoramazon.spring.app.entities.ImpuestoAduana;


@Repository
public interface ImpuestoAduanaRepository extends PagingAndSortingRepository<ImpuestoAduana, Long> {

}
