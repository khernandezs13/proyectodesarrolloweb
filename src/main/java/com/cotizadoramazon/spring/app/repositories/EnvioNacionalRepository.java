package com.cotizadoramazon.spring.app.repositories;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.cotizadoramazon.spring.app.entities.EnvioNacional;


@Repository
public interface EnvioNacionalRepository extends PagingAndSortingRepository<EnvioNacional, Long> {

}
