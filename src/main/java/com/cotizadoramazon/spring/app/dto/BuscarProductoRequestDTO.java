package com.cotizadoramazon.spring.app.dto;

import java.util.List;

public class BuscarProductoRequestDTO {

	private String productTitle;
	private String productDescription;
	private List<String> imageUrlList;
	private double price;

	
	

	public BuscarProductoRequestDTO(String productTitle, String productDescription, List<String> imageUrlList, double price) {
		super();
		this.productTitle = productTitle;
		this.productDescription = productDescription;
		this.imageUrlList = imageUrlList;
		this.price = price;
	}

	public String getProductTitle() {
		return productTitle;
	}

	public void setProductTitle(String productTitle) {
		this.productTitle = productTitle;
	}

	public List<String> getImageUrlList() {
		return imageUrlList;
	}

	public void setImageUrlList(List<String> imageUrlList) {
		this.imageUrlList = imageUrlList;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public String getProductDescription() {
		return productDescription;
	}

	public void setProductDescription(String productDescription) {
		this.productDescription = productDescription;
	}

	
	
	
	
}
