package com.cotizadoramazon.spring.app.dto;

public class IntercambioRequestDTO {
	
	private double result;

	public IntercambioRequestDTO(double result) {
		super();
		this.result = result;
	}

	public double getResult() {
		return result;
	}

	public void setResult(double result) {
		this.result = result;
	}
	
	

}
