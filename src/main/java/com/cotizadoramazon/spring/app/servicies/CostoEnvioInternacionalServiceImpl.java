package com.cotizadoramazon.spring.app.servicies;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cotizadoramazon.spring.app.entities.CostoEnvioInternacional;
import com.cotizadoramazon.spring.app.repositories.CostoEnvioInternacionalRepository;

@Service
public class CostoEnvioInternacionalServiceImpl implements CostoEnvioInternacionalService {

	@Autowired
	private CostoEnvioInternacionalRepository costoEnvioInternacionalRepository;
	
	
	@Override
	@Transactional(readOnly = true)
	public List<CostoEnvioInternacional> listarImporteEnvio() {
		
		return (List<CostoEnvioInternacional>) costoEnvioInternacionalRepository.findAll();
		
	}
    
	@Transactional
	@Override
	public void guardar(CostoEnvioInternacional costoEnvioInternacional) {
		
		costoEnvioInternacionalRepository.save(costoEnvioInternacional);
		
	}
	
	@Transactional
	@Override
	public void eliminar(CostoEnvioInternacional costoEnvioInternacional) {
		costoEnvioInternacionalRepository.delete(costoEnvioInternacional);
		
	}

	@Override
	@Transactional(readOnly = true)
	public CostoEnvioInternacional encontrarCostoEnvioInternacional(CostoEnvioInternacional costoEnvioInternacional) {
		
		return costoEnvioInternacionalRepository.findById(costoEnvioInternacional.getId()).orElse(null);
	}

}
