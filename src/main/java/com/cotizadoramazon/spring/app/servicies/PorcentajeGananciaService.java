package com.cotizadoramazon.spring.app.servicies;

import java.util.List;

import com.cotizadoramazon.spring.app.entities.PorcentajeGanancia;


public interface PorcentajeGananciaService {

	public List<PorcentajeGanancia>listarPorcentajeGanancia();
	
	public void guardar(PorcentajeGanancia porcentajeGanancia);
	
	public void eliminar(PorcentajeGanancia porcentajeGanancia);
	
	public PorcentajeGanancia encontrarPorcentaje(PorcentajeGanancia porcentajeGanancia);
}
