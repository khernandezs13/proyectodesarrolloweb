package com.cotizadoramazon.spring.app.servicies;

import java.util.List;

import com.cotizadoramazon.spring.app.entities.CostoEnvioInternacional;



 public interface CostoEnvioInternacionalService {

	public List<CostoEnvioInternacional>listarImporteEnvio();
	
	public void guardar(CostoEnvioInternacional costoEnvioInternacional);
	
	public void eliminar(CostoEnvioInternacional costoEnvioInternacional);
	
	public CostoEnvioInternacional encontrarCostoEnvioInternacional(CostoEnvioInternacional costoEnvioInternacional);
}
