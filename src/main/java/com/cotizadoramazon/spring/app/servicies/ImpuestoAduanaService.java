package com.cotizadoramazon.spring.app.servicies;

import java.util.List;

import com.cotizadoramazon.spring.app.entities.ImpuestoAduana;



public interface ImpuestoAduanaService {
	public List<ImpuestoAduana>listarImpuestoAduana();
	
	public void guardar(ImpuestoAduana impuestoAduana);
	
	public void eliminar(ImpuestoAduana impuestoAduana);
	
	public ImpuestoAduana encontrarImpuestoAduana(ImpuestoAduana impuestoAduana);
}




