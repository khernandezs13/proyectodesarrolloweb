package com.cotizadoramazon.spring.app.servicies;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cotizadoramazon.spring.app.entities.ImpuestoAduana;
import com.cotizadoramazon.spring.app.repositories.ImpuestoAduanaRepository;

@Service
public class ImpuestoAduanaServiceImpl implements ImpuestoAduanaService {
	
	@Autowired
	private ImpuestoAduanaRepository impuestoAduanaRepository;
	

	@Override
	@Transactional(readOnly = true)
	public List<ImpuestoAduana> listarImpuestoAduana() {
		
		return (List<ImpuestoAduana>) impuestoAduanaRepository.findAll();
	}

	@Transactional
	@Override
	public void guardar(ImpuestoAduana impuestoAduana) {	
		impuestoAduanaRepository.save(impuestoAduana);
	}

	@Transactional
	@Override
	public void eliminar(ImpuestoAduana impuestoAduana) {
		impuestoAduanaRepository.delete(impuestoAduana);
		
	}

	@Override
	@Transactional(readOnly = true)
	public ImpuestoAduana encontrarImpuestoAduana(ImpuestoAduana impuestoAduana) {
		return impuestoAduanaRepository.findById(impuestoAduana.getId()).orElse(null);
	}

}
