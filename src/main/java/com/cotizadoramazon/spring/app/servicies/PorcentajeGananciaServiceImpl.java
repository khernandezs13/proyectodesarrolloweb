package com.cotizadoramazon.spring.app.servicies;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import com.cotizadoramazon.spring.app.entities.PorcentajeGanancia;
import com.cotizadoramazon.spring.app.repositories.PorcentajeGananciaRepository;

@Service
public class PorcentajeGananciaServiceImpl implements PorcentajeGananciaService {

	@Autowired
	private PorcentajeGananciaRepository porcentajeGananciaRepository;
	
	@Override
	@Transactional(readOnly = true)
	public List<PorcentajeGanancia> listarPorcentajeGanancia() {
		return (List<PorcentajeGanancia>) porcentajeGananciaRepository.findAll();
	}

	@Transactional
	@Override
	public void guardar(PorcentajeGanancia porcentajeGanancia) {
		porcentajeGananciaRepository.save(porcentajeGanancia);
		
	}

	@Transactional
	@Override
	public void eliminar(PorcentajeGanancia porcentajeGanancia) {
		porcentajeGananciaRepository.delete(porcentajeGanancia);
		
	}

	@Override
	@Transactional(readOnly = true)
	public PorcentajeGanancia encontrarPorcentaje(PorcentajeGanancia porcentajeGanancia) {
		
		return porcentajeGananciaRepository.findById(porcentajeGanancia.getId()).orElse(null);
	}

}
