package com.cotizadoramazon.spring.app.servicies;

import java.util.List;

import com.cotizadoramazon.spring.app.entities.EnvioNacional;

public interface EnvioNacionalService {
	
	public List<EnvioNacional>listarEnvioNacional();
	
	public void guardar(EnvioNacional envioNacional);
	
	public void eliminar(EnvioNacional envioNacional);
	
	public EnvioNacional encontrarEnvioNacional(EnvioNacional envioNacional);
	
}
