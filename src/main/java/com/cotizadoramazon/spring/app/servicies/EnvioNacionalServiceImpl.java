package com.cotizadoramazon.spring.app.servicies;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cotizadoramazon.spring.app.entities.EnvioNacional;
import com.cotizadoramazon.spring.app.entities.ImpuestoAduana;
import com.cotizadoramazon.spring.app.repositories.EnvioNacionalRepository;
import com.cotizadoramazon.spring.app.repositories.ImpuestoAduanaRepository;

@Service
public class EnvioNacionalServiceImpl implements EnvioNacionalService  {

	@Autowired
	private EnvioNacionalRepository envioNacionalRepository;
	
	@Override
	@Transactional(readOnly = true)
	public List<EnvioNacional> listarEnvioNacional() {
		
		return (List<EnvioNacional>) envioNacionalRepository.findAll();
		
	}

	@Transactional
	@Override
	public void guardar(EnvioNacional envioNacional) {
		envioNacionalRepository.save(envioNacional);
		
	}

	@Transactional
	@Override
	public void eliminar(EnvioNacional envioNacional) {
		envioNacionalRepository.delete(envioNacional);
		
	}

	
	@Override
	@Transactional(readOnly = true)
	public EnvioNacional encontrarEnvioNacional(EnvioNacional envioNacional) {
		return envioNacionalRepository.findById(envioNacional.getId()).orElse(null);
	}

	
}
