package com.cotizadoramazon.spring.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CotizadorAmazon {

	public static void main(String[] args) {
		SpringApplication.run(CotizadorAmazon.class, args);
	}
}
