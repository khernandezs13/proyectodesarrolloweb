package com.cotizadoramazon.spring.app.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import com.cotizadoramazon.spring.app.entities.CostoEnvioInternacional;
import com.cotizadoramazon.spring.app.entities.EnvioNacional;
import com.cotizadoramazon.spring.app.entities.ImpuestoAduana;
import com.cotizadoramazon.spring.app.entities.PorcentajeGanancia;
import com.cotizadoramazon.spring.app.servicies.CostoEnvioInternacionalService;
import com.cotizadoramazon.spring.app.servicies.EnvioNacionalService;
import com.cotizadoramazon.spring.app.servicies.ImpuestoAduanaService;
import com.cotizadoramazon.spring.app.servicies.PorcentajeGananciaService;

@Controller
public class AdministracionController {

	@Autowired
	private PorcentajeGananciaService porcentajeGananciaService;

	@Autowired
	private ImpuestoAduanaService impuestoAduanaService;

	@Autowired
	private CostoEnvioInternacionalService costoEnvioInternacionalService;

	@Autowired
	private EnvioNacionalService envioNacionalService;

	@GetMapping("/administracion")
	public String inicio(Model model) {

		Iterable<PorcentajeGanancia> porcentGanancia = porcentajeGananciaService.listarPorcentajeGanancia();
		Iterable<ImpuestoAduana> impuestoAduana = impuestoAduanaService.listarImpuestoAduana();

		model.addAttribute("porcentGanancia", porcentGanancia);
		model.addAttribute("impuestoAduana", impuestoAduana);

		Iterable<CostoEnvioInternacional> costoEnvioInter = costoEnvioInternacionalService.listarImporteEnvio();
		model.addAttribute("costoEnvioInter", costoEnvioInter);

		Iterable<EnvioNacional> envioNacional = envioNacionalService.listarEnvioNacional();
		model.addAttribute("envioNacional", envioNacional);

		return "administracion";
	}

}
