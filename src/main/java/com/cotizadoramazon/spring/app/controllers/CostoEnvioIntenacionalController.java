package com.cotizadoramazon.spring.app.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import com.cotizadoramazon.spring.app.entities.CostoEnvioInternacional;
import com.cotizadoramazon.spring.app.servicies.CostoEnvioInternacionalService;


@Controller
public class CostoEnvioIntenacionalController {

	@Autowired
 private CostoEnvioInternacionalService costoEnvioInternacionalService;
	
	//sera una ventana modal a costo envio
	
		@GetMapping("/agregar-costo-envioUSAGUA")
		public String agregar(Model model) {
			
			CostoEnvioInternacional costoEnvioInter = new CostoEnvioInternacional();
			model.addAttribute("costoEnvioInter", costoEnvioInter);//tipoCambio va th:object
			
					
			return "envio-USAGUA"; 
		}
		
		
		//Metodo para guardar el tipo de cambio
		@PostMapping("/guardar-internacional")
		public String guardar(CostoEnvioInternacional costoEnvioInternacional) {
			
			costoEnvioInternacionalService.guardar(costoEnvioInternacional);
			
			return "redirect:/administracion";
			
		}
		
		//Eliminar 
		@GetMapping("/eliminar-envio-internacional/{id}")
		public String eliminar(CostoEnvioInternacional costoEnvioInternacional) {
			costoEnvioInternacionalService.eliminar(costoEnvioInternacional);
					
			return "redirect:/administracion";
		}
}
