package com.cotizadoramazon.spring.app.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import com.cotizadoramazon.spring.app.entities.CostoEnvioInternacional;
import com.cotizadoramazon.spring.app.entities.EnvioNacional;
import com.cotizadoramazon.spring.app.servicies.CostoEnvioInternacionalService;
import com.cotizadoramazon.spring.app.servicies.EnvioNacionalService;

@Controller
public class EnvioNacionalController {
	
	@Autowired
	 private EnvioNacionalService envioNacionalService;
	
	
	//sera una ventana modal a costo envio nacional 
	
			@GetMapping("/agregar-envio-nacional")
			public String agregar(Model model) {
				
				EnvioNacional envioNacional = new EnvioNacional();
				model.addAttribute("envioNacional", envioNacional);//tipoCambio va th:object
				
						
				return "costo-envio-nacional"; 
			}
	
			
			
			//Metodo para guardar el tipo de cambio
			@PostMapping("/guardar-envio-nacional")
			public String guardar(EnvioNacional envioNacional) {
				
				envioNacionalService.guardar(envioNacional);
				
				return "redirect:/administracion";
				
			}
			
			//Eliminar 
			@GetMapping("/eliminar-envio-nacional/{id}")
			public String eliminar(EnvioNacional envioNacional) {
				envioNacionalService.eliminar(envioNacional);
						
				return "redirect:/administracion";
			}
			
			
	

}
