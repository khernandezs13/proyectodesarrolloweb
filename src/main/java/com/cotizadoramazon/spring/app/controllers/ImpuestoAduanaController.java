package com.cotizadoramazon.spring.app.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import com.cotizadoramazon.spring.app.entities.ImpuestoAduana;
import com.cotizadoramazon.spring.app.servicies.ImpuestoAduanaService;

@Controller
public class ImpuestoAduanaController {

	@Autowired
 private ImpuestoAduanaService impuestoAduanaService;
	
	//sera una ventana modal a impuesto aduana
	
		@GetMapping("/agregar-impuesto-aduana")
		public String agregar(Model model) {
			
			ImpuestoAduana impuestoAduana = new ImpuestoAduana();
			model.addAttribute("impuestoAduana", impuestoAduana);//tipoCambio va th:object
			
					
			return "impuesto-aduana"; 
		}
		
		
		//Metodo para guardar el tipo de cambio
		@PostMapping("/guardar-impuesto-aduana")
		public String guardar(ImpuestoAduana impuestoAduana) {
			
			impuestoAduanaService.guardar(impuestoAduana);
			
			return "redirect:/administracion";
			
		}
		
		
		//Eliminar 
		@GetMapping("/eliminar-impuesto-aduana/{id}")
		public String eliminar(ImpuestoAduana impuestoAduana) {
			impuestoAduanaService.eliminar(impuestoAduana);
					
			return "redirect:/administracion";
		}
		
	
}
