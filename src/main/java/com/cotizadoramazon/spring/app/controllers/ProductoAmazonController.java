package com.cotizadoramazon.spring.app.controllers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;
import com.cotizadoramazon.spring.app.dto.IntercambioRequestDTO;
import com.cotizadoramazon.spring.app.dto.ProductoAmazon;
import com.cotizadoramazon.spring.app.dto.IntercambioResponseDTO;
import com.cotizadoramazon.spring.app.dto.BuscarProductoRequestDTO;
import com.cotizadoramazon.spring.app.dto.BuscarProductoResponseDTO;
import com.cotizadoramazon.spring.app.entities.CostoEnvioInternacional;
import com.cotizadoramazon.spring.app.entities.EnvioNacional;
import com.cotizadoramazon.spring.app.entities.ImpuestoAduana;
import com.cotizadoramazon.spring.app.entities.PorcentajeGanancia;
import com.cotizadoramazon.spring.app.servicies.CostoEnvioInternacionalService;
import com.cotizadoramazon.spring.app.servicies.EnvioNacionalService;
import com.cotizadoramazon.spring.app.servicies.ImpuestoAduanaService;
import com.cotizadoramazon.spring.app.servicies.PorcentajeGananciaService;



@Controller
public class ProductoAmazonController {
	
	@Autowired
    private PorcentajeGananciaService porcentajeGananciaService;
	@Autowired
    private ImpuestoAduanaService impuestoAduanaService;
	@Autowired
    private CostoEnvioInternacionalService costoEnvioInternacionalService;
	@Autowired
	private EnvioNacionalService envioNacionalService;

	
		
	@RequestMapping(value = "/mostrar-producto")
	public String mostrarProducto(@RequestParam(value = "url", required = false)String url, Model model) {
		
		
		if(url==null) {
			boolean estado = true;
			List<String> lista = new ArrayList<>();
			lista.add(0, "");
			ProductoAmazon productoAmazon = new ProductoAmazon();
			productoAmazon.setProductTitle(" ");
			productoAmazon.setProductDescription(" ");
			productoAmazon.setImageUrlList(lista);
			productoAmazon.setPrice(0.0);
			
			model.addAttribute("productoAmazon", productoAmazon);			
			model.addAttribute("estado",estado);
						
	        return "productos";
		}
		
		
	Iterable<PorcentajeGanancia> porcentGanancia = porcentajeGananciaService.listarPorcentajeGanancia();
	Iterable<ImpuestoAduana> impuestoAduana = impuestoAduanaService.listarImpuestoAduana();
	Iterable<CostoEnvioInternacional> costoEnvioInter = costoEnvioInternacionalService.listarImporteEnvio();		
	Iterable<EnvioNacional> envioNacional = envioNacionalService.listarEnvioNacional();	
	
	
	
	 //Realizando las distintas operaciones y conversiones
	
	double acumuladorTotal=0, ganancia=0, porAduana=0,envioInternacional=0, envioNac=0;
		
	
		for(PorcentajeGanancia gananciaPorcentaje : porcentGanancia) {
			ganancia = gananciaPorcentaje.getPorcentajeGanancia();
		}
		
		for(ImpuestoAduana impAduana : impuestoAduana) {
			porAduana = impAduana.getCantImpuestoAduana();
		}
		
		
		for(CostoEnvioInternacional internacional : costoEnvioInter) {
			envioInternacional = internacional.getCostoEnvioInternacional();
		}
		
		for(EnvioNacional nacional : envioNacional) {
			envioNac = nacional.getEnvioNacional();
		}
		

	  System.out.print("\n\nPorcentaje Ganancia: "+ganancia);
	  System.out.print("\nPorcentaje aduana: "+porAduana);
	  System.out.print("\nCosto Usa a Guatemala: "+envioInternacional);
	  System.out.print("\nCosto envio nacional: "+envioNac+"\n");
	  
	  		
		boolean estado = false;
		int numeroDP = url.indexOf("/dp/");			
		
		url = "https://www.amazon.com"+url.substring(numeroDP, numeroDP + 14);
		
		String url2 = "https://axesso-axesso-amazon-data-service-v1.p.rapidapi.com/amz/amazon-lookup-product";
				
		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders requestHeaders = new HttpHeaders();
		
		requestHeaders.set("x-rapidapi-host", "axesso-axesso-amazon-data-service-v1.p.rapidapi.com");
		requestHeaders.set("x-rapidapi-key", "761736b7f6msha7ee279ef70a59bp1a65d6jsnac7e6a012571"); // DAVID
		// requestHeaders.set("x-rapidapi-key", "b1e83049b2msh73dc9e92772245dp1f1519jsn247a3f397d72"); // KEYZER
		requestHeaders.setContentType(MediaType.APPLICATION_JSON);
		
		
        
        HttpEntity<BuscarProductoRequestDTO> requestEntity = new HttpEntity<>(requestHeaders);
        UriComponentsBuilder componentBuilder = UriComponentsBuilder.fromHttpUrl(url2).queryParam("url", url);
     
        ResponseEntity<BuscarProductoResponseDTO> responseEntity = 
      		  restTemplate.exchange( 
      		  componentBuilder.toUriString(),
      		  HttpMethod.GET, 
      		  requestEntity, 
      		BuscarProductoResponseDTO.class);
        
        BuscarProductoResponseDTO productoAmazon = responseEntity.getBody();      
        model.addAttribute("productoAmazon",productoAmazon);
        
        
        HttpHeaders requestHeaders2 = new HttpHeaders();
		
		requestHeaders2.set("x-rapidapi-host", "currency-conversion-and-exchange-rates.p.rapidapi.com");
		requestHeaders2.set("x-rapidapi-key", "b1e83049b2msh73dc9e92772245dp1f1519jsn247a3f397d72");
		requestHeaders2.setContentType(MediaType.APPLICATION_JSON);
		
		String url3 = "https://currency-conversion-and-exchange-rates.p.rapidapi.com/convert";
        double precio = productoAmazon.getPrice();
		
        HttpEntity<IntercambioRequestDTO> requestEntity2 = new HttpEntity<>(requestHeaders2);
        UriComponentsBuilder builder2 = UriComponentsBuilder.fromHttpUrl(url3)
                .queryParam("from", "USD").queryParam("to", "GTQ").queryParam("amount", precio);
     
        ResponseEntity<IntercambioResponseDTO> responseEntity2 = 
      		  restTemplate.exchange(builder2.toUriString(),HttpMethod.GET,requestEntity2, IntercambioResponseDTO.class);
        
        IntercambioResponseDTO conversionPrecio = responseEntity2.getBody();	        
        
       
        double converPorcentajeGanancia=0, converImpuestoAdu=0, gananciaPrecio=0, impueAduanaTotal = 0, precioAmazon=0,sumaGananciaPrecio=0,totalFinal=0;
        
        converPorcentajeGanancia = (ganancia /100);
        converImpuestoAdu = (porAduana / 100);
        
		acumuladorTotal=(envioInternacional+envioNac);
          
        precioAmazon = conversionPrecio.getResult();
        
        
         gananciaPrecio = (converPorcentajeGanancia*precioAmazon);
         
         impueAduanaTotal = (converImpuestoAdu * precioAmazon);
         
         
         sumaGananciaPrecio =(gananciaPrecio + impueAduanaTotal + precioAmazon);
        
        totalFinal=(acumuladorTotal + sumaGananciaPrecio);
        
      System.out.print("\nPorcentaje: "+converPorcentajeGanancia);
      System.out.print("\nImpuesto Aduana: "+converPorcentajeGanancia);
      System.out.print("\nEnvio y costos de envio: "+impueAduanaTotal);
  	  System.out.print("\nTotal de Ganancia: "+gananciaPrecio);
  	  System.out.print("\nPrecio final: "+totalFinal+"\n\n");
        
        
        String precioFinal = "Q "+ String.format("%.2f",totalFinal);
        
        model.addAttribute("precioFinal", precioFinal);
        model.addAttribute("estado", estado);
        

        return "productos";
	   
	}
	
	
		
		
}
