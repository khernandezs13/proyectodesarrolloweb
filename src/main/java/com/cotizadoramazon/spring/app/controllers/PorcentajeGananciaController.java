package com.cotizadoramazon.spring.app.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import com.cotizadoramazon.spring.app.entities.PorcentajeGanancia;
import com.cotizadoramazon.spring.app.servicies.PorcentajeGananciaService;

@Controller
public class PorcentajeGananciaController {

	@Autowired
 private PorcentajeGananciaService porcentajeGananciaService;
	
	//sera una ventana modal a porcentaje ganancia
	
	@GetMapping("/agregar-porcentaje-ganancia")
	public String agregar(Model model) {
		
		PorcentajeGanancia porcentGanancia = new PorcentajeGanancia();
		model.addAttribute("porcentGanancia", porcentGanancia);//tipoCambio va th:object
		
				
		return "porcentaje-ganancia"; 
	}
	
	
	//Metodo para guardar el tipo de cambio
			@PostMapping("/guardar-porcentaje-ganancia")
			public String guardar(PorcentajeGanancia porcentGanancia) {
				
				porcentajeGananciaService.guardar(porcentGanancia);
				
				return "redirect:/administracion";
				
			}
			
			//Eliminar 
			@GetMapping("/eliminar-porcentaje-ganancia/{id}")
			public String eliminar(PorcentajeGanancia porcentGanancia) {
				porcentajeGananciaService.eliminar(porcentGanancia);
						
				return "redirect:/administracion";
			}
		
}
